USE [EmployeesDb]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2020-03-12 18:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](25) NOT NULL,
	[Surname] [nvarchar](25) NOT NULL,
	[DateOfEmployment] [datetime] NOT NULL,
	[Salary] [money] NOT NULL,
	[PositionId] [int] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeSkills]    Script Date: 2020-03-12 18:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSkills](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[SkillId] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeSkills] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 2020-03-12 18:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Positions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PositionSkills]    Script Date: 2020-03-12 18:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionSkills](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PositionId] [int] NOT NULL,
	[SkillId] [int] NOT NULL,
 CONSTRAINT [PK_PositionSkills] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Skills]    Script Date: 2020-03-12 18:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Skills](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_Skills] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 
GO
INSERT [dbo].[Employees] ([Id], [Name], [Surname], [DateOfEmployment], [Salary], [PositionId]) VALUES (1, N'John', N'Doe', CAST(N'2020-03-09T16:11:36.227' AS DateTime), 100.0000, 1)
GO
INSERT [dbo].[Employees] ([Id], [Name], [Surname], [DateOfEmployment], [Salary], [PositionId]) VALUES (2, N'Jane', N'Doe', CAST(N'2020-03-09T16:11:51.220' AS DateTime), 120.0000, 1)
GO
INSERT [dbo].[Employees] ([Id], [Name], [Surname], [DateOfEmployment], [Salary], [PositionId]) VALUES (4, N'Kirill', N'Dudarenko', CAST(N'2020-03-09T00:00:00.000' AS DateTime), 125.0000, 1)
GO
SET IDENTITY_INSERT [dbo].[Employees] OFF
GO
SET IDENTITY_INSERT [dbo].[EmployeeSkills] ON 
GO
INSERT [dbo].[EmployeeSkills] ([Id], [EmployeeId], [SkillId]) VALUES (1, 1, 1)
GO
INSERT [dbo].[EmployeeSkills] ([Id], [EmployeeId], [SkillId]) VALUES (3, 2, 3)
GO
INSERT [dbo].[EmployeeSkills] ([Id], [EmployeeId], [SkillId]) VALUES (4, 2, 2)
GO
INSERT [dbo].[EmployeeSkills] ([Id], [EmployeeId], [SkillId]) VALUES (5, 4, 2)
GO
SET IDENTITY_INSERT [dbo].[EmployeeSkills] OFF
GO
SET IDENTITY_INSERT [dbo].[Positions] ON 
GO
INSERT [dbo].[Positions] ([Id], [Name]) VALUES (1, N'Software Developer')
GO
INSERT [dbo].[Positions] ([Id], [Name]) VALUES (2, N'UI/UX Designer')
GO
SET IDENTITY_INSERT [dbo].[Positions] OFF
GO
SET IDENTITY_INSERT [dbo].[PositionSkills] ON 
GO
INSERT [dbo].[PositionSkills] ([Id], [PositionId], [SkillId]) VALUES (1, 1, 1)
GO
INSERT [dbo].[PositionSkills] ([Id], [PositionId], [SkillId]) VALUES (2, 1, 2)
GO
INSERT [dbo].[PositionSkills] ([Id], [PositionId], [SkillId]) VALUES (3, 1, 3)
GO
INSERT [dbo].[PositionSkills] ([Id], [PositionId], [SkillId]) VALUES (4, 2, 4)
GO
INSERT [dbo].[PositionSkills] ([Id], [PositionId], [SkillId]) VALUES (5, 2, 5)
GO
SET IDENTITY_INSERT [dbo].[PositionSkills] OFF
GO
SET IDENTITY_INSERT [dbo].[Skills] ON 
GO
INSERT [dbo].[Skills] ([Id], [Name]) VALUES (1, N'C++')
GO
INSERT [dbo].[Skills] ([Id], [Name]) VALUES (2, N'C#')
GO
INSERT [dbo].[Skills] ([Id], [Name]) VALUES (3, N'VB')
GO
INSERT [dbo].[Skills] ([Id], [Name]) VALUES (4, N'xaml')
GO
INSERT [dbo].[Skills] ([Id], [Name]) VALUES (5, N'css')
GO
SET IDENTITY_INSERT [dbo].[Skills] OFF
GO
ALTER TABLE [dbo].[Employees] ADD  CONSTRAINT [DF_Employees_DateOfEmployment]  DEFAULT (getdate()) FOR [DateOfEmployment]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Positions] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Positions] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Positions]
GO
ALTER TABLE [dbo].[EmployeeSkills]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeSkills_Employees_EmployeeId] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employees] ([Id])
GO
ALTER TABLE [dbo].[EmployeeSkills] CHECK CONSTRAINT [FK_EmployeeSkills_Employees_EmployeeId]
GO
ALTER TABLE [dbo].[EmployeeSkills]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeSkills_Skills_SkillId] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skills] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EmployeeSkills] CHECK CONSTRAINT [FK_EmployeeSkills_Skills_SkillId]
GO
ALTER TABLE [dbo].[PositionSkills]  WITH CHECK ADD  CONSTRAINT [FK_PositionSkills_Positions_PositionId] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Positions] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PositionSkills] CHECK CONSTRAINT [FK_PositionSkills_Positions_PositionId]
GO
ALTER TABLE [dbo].[PositionSkills]  WITH CHECK ADD  CONSTRAINT [FK_PositionSkills_Skills_SkillId] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skills] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PositionSkills] CHECK CONSTRAINT [FK_PositionSkills_Skills_SkillId]
GO
