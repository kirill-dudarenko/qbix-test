﻿using dal.Entities;
using System.Data.Linq.Mapping;

namespace dal
{
	public partial class EmployeeContext : System.Data.Linq.DataContext
	{
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();

		#region Extensibility Method Definitions
		partial void OnCreated();
		partial void InsertEmployee(Employee instance);
		partial void UpdateEmployee(Employee instance);
		partial void DeleteEmployee(Employee instance);
		partial void InsertEmployeeSkill(EmployeeSkill instance);
		partial void UpdateEmployeeSkill(EmployeeSkill instance);
		partial void DeleteEmployeeSkill(EmployeeSkill instance);
		partial void InsertPosition(Position instance);
		partial void UpdatePosition(Position instance);
		partial void DeletePosition(Position instance);
		partial void InsertPositionSkill(PositionSkill instance);
		partial void UpdatePositionSkill(PositionSkill instance);
		partial void DeletePositionSkill(PositionSkill instance);
		partial void InsertSkill(Skill instance);
		partial void UpdateSkill(Skill instance);
		partial void DeleteSkill(Skill instance);
		#endregion

		public EmployeeContext(string connection) :
				base(connection, mappingSource)
		{
			OnCreated();
		}

		public EmployeeContext(System.Data.IDbConnection connection) :
				base(connection, mappingSource)
		{
			OnCreated();
		}

		public EmployeeContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) :
				base(connection, mappingSource)
		{
			OnCreated();
		}

		public EmployeeContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) :
				base(connection, mappingSource)
		{
			OnCreated();
		}

		public System.Data.Linq.Table<Employee> Employees
		{
			get
			{
				return this.GetTable<Employee>();
			}
		}

		public System.Data.Linq.Table<EmployeeSkill> EmployeeSkills
		{
			get
			{
				return this.GetTable<EmployeeSkill>();
			}
		}

		public System.Data.Linq.Table<Position> Positions
		{
			get
			{
				return this.GetTable<Position>();
			}
		}

		public System.Data.Linq.Table<PositionSkill> PositionSkills
		{
			get
			{
				return this.GetTable<PositionSkill>();
			}
		}

		public System.Data.Linq.Table<Skill> Skills
		{
			get
			{
				return this.GetTable<Skill>();
			}
		}
	}
}
