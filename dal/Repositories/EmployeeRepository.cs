﻿using dal.Entities;
using dal.Interfaces;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace dal.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly string _connection;

        public EmployeeRepository(string connection)
        {
            _connection = connection;
        }

        public void AddEmployee(Employee newEmployee)
        {
            using (var db = new EmployeeContext(_connection))
            {
                db.Employees.InsertOnSubmit(newEmployee);
                db.SubmitChanges();
            }
        }

        public void DeleteEmployee(int id)
        {
            using (var db = new EmployeeContext(_connection))
            {
                var dbEmployee = db.Employees.FirstOrDefault(x => x.Id == id);
                if (dbEmployee != null)
                {
                    db.Employees.DeleteOnSubmit(dbEmployee);
                    db.SubmitChanges();
                }
            }
        }

        public Employee GetEmployee(int id)
        {
            using (var db = new EmployeeContext(_connection))
            {
                var dlo = new DataLoadOptions();
                dlo.LoadWith<Employee>(x => x.Position);
                dlo.LoadWith<Employee>(x => x.EmployeeSkills);
                dlo.LoadWith<EmployeeSkill>(x => x.Skill);
                db.LoadOptions = dlo;

                return db.Employees.FirstOrDefault(x => x.Id == id);
            }
        }

        public IEnumerable<Employee> GetEmployees()
        {
            using (var db = new EmployeeContext(_connection))
            {
                var dlo = new DataLoadOptions();
                dlo.LoadWith<Employee>(x => x.Position);
                db.LoadOptions = dlo;

                return db.Employees.ToList();
            }
        }

        public void UpdateEmployee(Employee employee)
        {
            using (var db = new EmployeeContext(_connection))
            {
                var dbEmployee = db.Employees.FirstOrDefault(x => x.Id == employee.Id);

                if (dbEmployee != null)
                {
                    db.Employees.DeleteOnSubmit(dbEmployee);
                }

                db.Employees.InsertOnSubmit(employee);
                db.SubmitChanges();
            }
        }
    }
}
