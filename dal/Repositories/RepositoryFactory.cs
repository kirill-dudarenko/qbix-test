﻿using dal.Interfaces;
using System;

namespace dal.Repositories
{
    public class RepositoryFactory
    {
        private static string Connection;
        public static void InitConnectionStore(string connection)
        {
            Connection = connection;
        }

        private void CheckConnectionInitialized()
        {
            if (string.IsNullOrEmpty(Connection))
                throw new ApplicationException("Connection is not initialized. Call StoreConnection method first.");
        }

        public IPositionRepository GetPositionRepository()
        {
            CheckConnectionInitialized();

            return new PositionRepository(Connection);
        }

        public IEmployeeRepository GetEmployeeRepository()
        {
            CheckConnectionInitialized();

            return new EmployeeRepository(Connection);
        }
    }
}
