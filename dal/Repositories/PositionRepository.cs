﻿using dal.Entities;
using dal.Interfaces;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace dal.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly string _connection;

        public PositionRepository(string connection)
        {
            _connection = connection;
        }

        public IEnumerable<Position> GetPositions()
        {
            using (var db = new EmployeeContext(_connection))
            {
                var dlo = new DataLoadOptions();
                dlo.LoadWith<Position>(x => x.PositionSkills);
                dlo.LoadWith<PositionSkill>(x => x.Skill);
                db.LoadOptions = dlo;

                return db.Positions.ToList();
            }
        }
    }
}
