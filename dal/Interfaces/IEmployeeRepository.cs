﻿using dal.Entities;
using System.Collections.Generic;

namespace dal.Interfaces
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployees();
        Employee GetEmployee(int id);
        void DeleteEmployee(int id);
        void AddEmployee(Employee newEmployee);
        void UpdateEmployee(Employee employee);
    }
}
