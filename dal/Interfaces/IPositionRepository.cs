﻿using dal.Entities;
using System.Collections.Generic;

namespace dal.Interfaces
{
    public interface IPositionRepository
    {
        IEnumerable<Position> GetPositions();
    }
}
