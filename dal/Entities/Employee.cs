﻿using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace dal.Entities
{
	[Table(Name = "dbo.Employees")]
	public partial class Employee : INotifyPropertyChanging, INotifyPropertyChanged
	{

		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

		private int _Id;

		private string _Name;

		private string _Surname;

		private System.DateTime _DateOfEmployment;

		private decimal _Salary;

		private int _PositionId;

		private EntityRef<Position> _Position;

		private EntitySet<EmployeeSkill> _EmployeeSkills;

		#region Extensibility Method Definitions
		partial void OnLoaded();
		partial void OnValidate(System.Data.Linq.ChangeAction action);
		partial void OnCreated();
		partial void OnIdChanging(int value);
		partial void OnIdChanged();
		partial void OnNameChanging(string value);
		partial void OnNameChanged();
		partial void OnSurnameChanging(string value);
		partial void OnSurnameChanged();
		partial void OnDateOfEmploymentChanging(System.DateTime value);
		partial void OnDateOfEmploymentChanged();
		partial void OnSalaryChanging(decimal value);
		partial void OnSalaryChanged();
		partial void OnPositionIdChanging(int value);
		partial void OnPositionIdChanged();
		#endregion

		public Employee()
		{
			this._Position = default(EntityRef<Position>);
			this._EmployeeSkills = new EntitySet<EmployeeSkill>(new Action<EmployeeSkill>(this.attach_EmployeeSkills), new Action<EmployeeSkill>(this.detach_EmployeeSkills));
			OnCreated();
		}

		[Column(Storage = "_Id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}

		[Column(Storage = "_Name", DbType = "NVarChar(25) NOT NULL", CanBeNull = false)]
		public string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				if ((this._Name != value))
				{
					this.OnNameChanging(value);
					this.SendPropertyChanging();
					this._Name = value;
					this.SendPropertyChanged("Name");
					this.OnNameChanged();
				}
			}
		}

		[Column(Storage = "_Surname", DbType = "NVarChar(25) NOT NULL", CanBeNull = false)]
		public string Surname
		{
			get
			{
				return this._Surname;
			}
			set
			{
				if ((this._Surname != value))
				{
					this.OnSurnameChanging(value);
					this.SendPropertyChanging();
					this._Surname = value;
					this.SendPropertyChanged("Surname");
					this.OnSurnameChanged();
				}
			}
		}

		[Column(Storage = "_DateOfEmployment", DbType = "DateTime NOT NULL")]
		public System.DateTime DateOfEmployment
		{
			get
			{
				return this._DateOfEmployment;
			}
			set
			{
				if ((this._DateOfEmployment != value))
				{
					this.OnDateOfEmploymentChanging(value);
					this.SendPropertyChanging();
					this._DateOfEmployment = value;
					this.SendPropertyChanged("DateOfEmployment");
					this.OnDateOfEmploymentChanged();
				}
			}
		}

		[Column(Storage = "_Salary", DbType = "Money NOT NULL")]
		public decimal Salary
		{
			get
			{
				return this._Salary;
			}
			set
			{
				if ((this._Salary != value))
				{
					this.OnSalaryChanging(value);
					this.SendPropertyChanging();
					this._Salary = value;
					this.SendPropertyChanged("Salary");
					this.OnSalaryChanged();
				}
			}
		}

		[Column(Storage = "_PositionId", DbType = "Int NOT NULL")]
		public int PositionId
		{
			get
			{
				return this._PositionId;
			}
			set
			{
				if ((this._PositionId != value))
				{
					if (this._Position.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnPositionIdChanging(value);
					this.SendPropertyChanging();
					this._PositionId = value;
					this.SendPropertyChanged("PositionId");
					this.OnPositionIdChanged();
				}
			}
		}

		[Association(Name = "FK_Employees_Positions", Storage = "_Position", ThisKey = "PositionId", OtherKey = "Id", IsForeignKey = true, DeleteOnNull = true)]
		public Position Position
		{
			get
			{
				return this._Position.Entity;
			}
			set
			{
				Position previousValue = this._Position.Entity;
				if (((previousValue != value)
							|| (this._Position.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Position.Entity = null;
						previousValue.Employees.Remove(this);
					}
					this._Position.Entity = value;
					if ((value != null))
					{
						value.Employees.Add(this);
						this._PositionId = value.Id;
					}
					else
					{
						this._PositionId = default(int);
					}
					this.SendPropertyChanged("Positions");
				}
			}
		}

		[Association(Name = "FK_EmployeeSkills_Employees_EmployeeId", Storage = "_EmployeeSkills", ThisKey = "Id", OtherKey = "EmployeeId", DeleteRule = "NO ACTION")]
		public EntitySet<EmployeeSkill> EmployeeSkills
		{
			get
			{
				return this._EmployeeSkills;
			}
			set
			{
				this._EmployeeSkills.Assign(value);
			}
		}

		public event PropertyChangingEventHandler PropertyChanging;

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}

		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		private void attach_EmployeeSkills(EmployeeSkill entity)
		{
			this.SendPropertyChanging();
			entity.Employees = this;
		}

		private void detach_EmployeeSkills(EmployeeSkill entity)
		{
			this.SendPropertyChanging();
			entity.Employees = null;
		}
	}
}
