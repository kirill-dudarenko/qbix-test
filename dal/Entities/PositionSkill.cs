﻿using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace dal.Entities
{
	[Table(Name = "dbo.PositionSkills")]
	public partial class PositionSkill : INotifyPropertyChanging, INotifyPropertyChanged
	{

		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

		private int _Id;

		private int _PositionId;

		private int _SkillId;

		private EntityRef<Position> _Position;

		private EntityRef<Skill> _Skill;

		#region Extensibility Method Definitions
		partial void OnLoaded();
		partial void OnValidate(System.Data.Linq.ChangeAction action);
		partial void OnCreated();
		partial void OnIdChanging(int value);
		partial void OnIdChanged();
		partial void OnPositionIdChanging(int value);
		partial void OnPositionIdChanged();
		partial void OnSkillIdChanging(int value);
		partial void OnSkillIdChanged();
		#endregion

		public PositionSkill()
		{
			this._Position = default(EntityRef<Position>);
			this._Skill = default(EntityRef<Skill>);
			OnCreated();
		}

		[Column(Storage = "_Id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}

		[Column(Storage = "_PositionId", DbType = "Int NOT NULL")]
		public int PositionId
		{
			get
			{
				return this._PositionId;
			}
			set
			{
				if ((this._PositionId != value))
				{
					if (this._Position.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnPositionIdChanging(value);
					this.SendPropertyChanging();
					this._PositionId = value;
					this.SendPropertyChanged("PositionId");
					this.OnPositionIdChanged();
				}
			}
		}

		[Column(Storage = "_SkillId", DbType = "Int NOT NULL")]
		public int SkillId
		{
			get
			{
				return this._SkillId;
			}
			set
			{
				if ((this._SkillId != value))
				{
					if (this._Skill.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnSkillIdChanging(value);
					this.SendPropertyChanging();
					this._SkillId = value;
					this.SendPropertyChanged("SkillId");
					this.OnSkillIdChanged();
				}
			}
		}

		[Association(Name = "FK_PositionSkills_Positions_PositionId", Storage = "_Position", ThisKey = "PositionId", OtherKey = "Id", IsForeignKey = true, DeleteOnNull = true)]
		public Position Position
		{
			get
			{
				return this._Position.Entity;
			}
			set
			{
				Position previousValue = this._Position.Entity;
				if (((previousValue != value)
							|| (this._Position.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Position.Entity = null;
						previousValue.PositionSkills.Remove(this);
					}
					this._Position.Entity = value;
					if ((value != null))
					{
						value.PositionSkills.Add(this);
						this._PositionId = value.Id;
					}
					else
					{
						this._PositionId = default(int);
					}
					this.SendPropertyChanged("Positions");
				}
			}
		}

		[Association(Name = "FK_PositionSkills_Skills_SkillId", Storage = "_Skill", ThisKey = "SkillId", OtherKey = "Id", IsForeignKey = true, DeleteOnNull = true)]
		public Skill Skill
		{
			get
			{
				return this._Skill.Entity;
			}
			set
			{
				Skill previousValue = this._Skill.Entity;
				if (((previousValue != value)
							|| (this._Skill.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Skill.Entity = null;
						previousValue.PositionSkills.Remove(this);
					}
					this._Skill.Entity = value;
					if ((value != null))
					{
						value.PositionSkills.Add(this);
						this._SkillId = value.Id;
					}
					else
					{
						this._SkillId = default(int);
					}
					this.SendPropertyChanged("Skill");
				}
			}
		}

		public event PropertyChangingEventHandler PropertyChanging;

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}

		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
