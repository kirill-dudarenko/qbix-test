﻿using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace dal.Entities
{
	[Table(Name = "dbo.EmployeeSkills")]
	public partial class EmployeeSkill : INotifyPropertyChanging, INotifyPropertyChanged
	{

		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

		private int _Id;

		private int _EmployeeId;

		private int _SkillId;

		private EntityRef<Employee> _Employee;

		private EntityRef<Skill> _Skill;

		#region Extensibility Method Definitions
		partial void OnLoaded();
		partial void OnValidate(System.Data.Linq.ChangeAction action);
		partial void OnCreated();
		partial void OnIdChanging(int value);
		partial void OnIdChanged();
		partial void OnEmployeeIdChanging(int value);
		partial void OnEmployeeIdChanged();
		partial void OnSkillIdChanging(int value);
		partial void OnSkillIdChanged();
		#endregion

		public EmployeeSkill()
		{
			this._Employee = default(EntityRef<Employee>);
			this._Skill = default(EntityRef<Skill>);
			OnCreated();
		}

		[Column(Storage = "_Id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}

		[Column(Storage = "_EmployeeId", DbType = "Int NOT NULL")]
		public int EmployeeId
		{
			get
			{
				return this._EmployeeId;
			}
			set
			{
				if ((this._EmployeeId != value))
				{
					if (this._Employee.HasLoadedOrAssignedValue)
					{
						throw new ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnEmployeeIdChanging(value);
					this.SendPropertyChanging();
					this._EmployeeId = value;
					this.SendPropertyChanged("EmployeeId");
					this.OnEmployeeIdChanged();
				}
			}
		}

		[Column(Storage = "_SkillId", DbType = "Int NOT NULL")]
		public int SkillId
		{
			get
			{
				return this._SkillId;
			}
			set
			{
				if ((this._SkillId != value))
				{
					if (this._Skill.HasLoadedOrAssignedValue)
					{
						throw new ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnSkillIdChanging(value);
					this.SendPropertyChanging();
					this._SkillId = value;
					this.SendPropertyChanged("SkillId");
					this.OnSkillIdChanged();
				}
			}
		}

		[Association(Name = "FK_EmployeeSkills_Employees_EmployeeId", Storage = "_Employee", ThisKey = "EmployeeId", OtherKey = "Id", IsForeignKey = true)]
		public Employee Employees
		{
			get
			{
				return this._Employee.Entity;
			}
			set
			{
				Employee previousValue = this._Employee.Entity;
				if (((previousValue != value)
							|| (this._Employee.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Employee.Entity = null;
						previousValue.EmployeeSkills.Remove(this);
					}
					this._Employee.Entity = value;
					if ((value != null))
					{
						value.EmployeeSkills.Add(this);
						this._EmployeeId = value.Id;
					}
					else
					{
						this._EmployeeId = default(int);
					}
					this.SendPropertyChanged("Employees");
				}
			}
		}

		[Association(Name = "FK_EmployeeSkills_Skills_SkillId", Storage = "_Skill", ThisKey = "SkillId", OtherKey = "Id", IsForeignKey = true, DeleteOnNull = true)]
		public Skill Skill
		{
			get
			{
				return this._Skill.Entity;
			}
			set
			{
				Skill previousValue = this._Skill.Entity;
				if (((previousValue != value)
							|| (this._Skill.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Skill.Entity = null;
						previousValue.EmployeeSkills.Remove(this);
					}
					this._Skill.Entity = value;
					if ((value != null))
					{
						value.EmployeeSkills.Add(this);
						this._SkillId = value.Id;
					}
					else
					{
						this._SkillId = default(int);
					}
					this.SendPropertyChanged("Skills");
				}
			}
		}

		public event PropertyChangingEventHandler PropertyChanging;

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}

		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
