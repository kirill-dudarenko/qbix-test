﻿using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace dal.Entities
{
	[Table(Name = "dbo.Positions")]
	public partial class Position : INotifyPropertyChanging, INotifyPropertyChanged
	{

		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

		private int _Id;

		private string _Name;

		private EntitySet<Employee> _Employees;

		private EntitySet<PositionSkill> _PositionSkills;

		#region Extensibility Method Definitions
		partial void OnLoaded();
		partial void OnValidate(System.Data.Linq.ChangeAction action);
		partial void OnCreated();
		partial void OnIdChanging(int value);
		partial void OnIdChanged();
		partial void OnNameChanging(string value);
		partial void OnNameChanged();
		#endregion

		public Position()
		{
			this._Employees = new EntitySet<Employee>(new Action<Employee>(this.attach_Employees), new Action<Employee>(this.detach_Employees));
			this._PositionSkills = new EntitySet<PositionSkill>(new Action<PositionSkill>(this.attach_PositionSkills), new Action<PositionSkill>(this.detach_PositionSkills));
			OnCreated();
		}

		[Column(Storage = "_Id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}

		[Column(Storage = "_Name", DbType = "NVarChar(25) NOT NULL", CanBeNull = false)]
		public string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				if ((this._Name != value))
				{
					this.OnNameChanging(value);
					this.SendPropertyChanging();
					this._Name = value;
					this.SendPropertyChanged("Name");
					this.OnNameChanged();
				}
			}
		}

		[Association(Name = "FK_Employees_Positions", Storage = "_Employees", ThisKey = "Id", OtherKey = "PositionId", DeleteRule = "CASCADE")]
		public EntitySet<Employee> Employees
		{
			get
			{
				return this._Employees;
			}
			set
			{
				this._Employees.Assign(value);
			}
		}

		[Association(Name = "FK_PositionSkills_Positions_PositionId", Storage = "_PositionSkills", ThisKey = "Id", OtherKey = "PositionId", DeleteRule = "CASCADE")]
		public EntitySet<PositionSkill> PositionSkills
		{
			get
			{
				return this._PositionSkills;
			}
			set
			{
				this._PositionSkills.Assign(value);
			}
		}

		public event PropertyChangingEventHandler PropertyChanging;

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}

		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		private void attach_Employees(Employee entity)
		{
			this.SendPropertyChanging();
			entity.Position = this;
		}

		private void detach_Employees(Employee entity)
		{
			this.SendPropertyChanging();
			entity.Position = null;
		}

		private void attach_PositionSkills(PositionSkill entity)
		{
			this.SendPropertyChanging();
			entity.Position = this;
		}

		private void detach_PositionSkills(PositionSkill entity)
		{
			this.SendPropertyChanging();
			entity.Position = null;
		}
	}
}
