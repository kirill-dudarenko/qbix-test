﻿namespace winui
{
    partial class NewEmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxPositions = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.uxSurname = new System.Windows.Forms.TextBox();
            this.uxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uxOk = new System.Windows.Forms.Button();
            this.uxCancel = new System.Windows.Forms.Button();
            this.uxEmploymentDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.uxSalary = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uxSkillsSelector = new winui.SkillsSelector();
            ((System.ComponentModel.ISupportInitialize)(this.uxSalary)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uxPositions
            // 
            this.uxPositions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxPositions.FormattingEnabled = true;
            this.uxPositions.Location = new System.Drawing.Point(12, 21);
            this.uxPositions.Name = "uxPositions";
            this.uxPositions.Size = new System.Drawing.Size(257, 21);
            this.uxPositions.TabIndex = 0;
            this.uxPositions.SelectedValueChanged += new System.EventHandler(this.uxPositions_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Position";
            // 
            // uxSurname
            // 
            this.uxSurname.Location = new System.Drawing.Point(284, 69);
            this.uxSurname.MaxLength = 25;
            this.uxSurname.Name = "uxSurname";
            this.uxSurname.Size = new System.Drawing.Size(257, 20);
            this.uxSurname.TabIndex = 2;
            // 
            // uxName
            // 
            this.uxName.Location = new System.Drawing.Point(284, 22);
            this.uxName.MaxLength = 25;
            this.uxName.Name = "uxName";
            this.uxName.Size = new System.Drawing.Size(257, 20);
            this.uxName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Surname";
            // 
            // uxOk
            // 
            this.uxOk.Location = new System.Drawing.Point(466, 356);
            this.uxOk.Name = "uxOk";
            this.uxOk.Size = new System.Drawing.Size(75, 23);
            this.uxOk.TabIndex = 3;
            this.uxOk.Text = "Ok";
            this.uxOk.UseVisualStyleBackColor = true;
            this.uxOk.Click += new System.EventHandler(this.uxOk_Click);
            // 
            // uxCancel
            // 
            this.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uxCancel.Location = new System.Drawing.Point(284, 356);
            this.uxCancel.Name = "uxCancel";
            this.uxCancel.Size = new System.Drawing.Size(75, 23);
            this.uxCancel.TabIndex = 3;
            this.uxCancel.Text = "Cancel";
            this.uxCancel.UseVisualStyleBackColor = true;
            // 
            // uxEmploymentDate
            // 
            this.uxEmploymentDate.Location = new System.Drawing.Point(284, 114);
            this.uxEmploymentDate.Name = "uxEmploymentDate";
            this.uxEmploymentDate.Size = new System.Drawing.Size(257, 20);
            this.uxEmploymentDate.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(281, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Employment Date";
            // 
            // uxSalary
            // 
            this.uxSalary.Location = new System.Drawing.Point(284, 168);
            this.uxSalary.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.uxSalary.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uxSalary.Name = "uxSalary";
            this.uxSalary.Size = new System.Drawing.Size(257, 20);
            this.uxSalary.TabIndex = 5;
            this.uxSalary.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(281, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Salary";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.uxSkillsSelector);
            this.panel1.Location = new System.Drawing.Point(12, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(257, 331);
            this.panel1.TabIndex = 7;
            // 
            // uxSkillsSelector
            // 
            this.uxSkillsSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSkillsSelector.Location = new System.Drawing.Point(0, 0);
            this.uxSkillsSelector.Name = "uxSkillsSelector";
            this.uxSkillsSelector.Size = new System.Drawing.Size(257, 331);
            this.uxSkillsSelector.TabIndex = 8;
            // 
            // NewEmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.uxCancel;
            this.ClientSize = new System.Drawing.Size(561, 391);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.uxSalary);
            this.Controls.Add(this.uxEmploymentDate);
            this.Controls.Add(this.uxCancel);
            this.Controls.Add(this.uxOk);
            this.Controls.Add(this.uxName);
            this.Controls.Add(this.uxSurname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.uxPositions);
            this.Name = "NewEmployeeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New Employee";
            this.Load += new System.EventHandler(this.NewEmployeeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uxSalary)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox uxPositions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox uxSurname;
        private System.Windows.Forms.TextBox uxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button uxOk;
        private System.Windows.Forms.Button uxCancel;
        private System.Windows.Forms.DateTimePicker uxEmploymentDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown uxSalary;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private SkillsSelector uxSkillsSelector;
    }
}