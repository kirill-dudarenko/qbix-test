﻿using dal.Entities;
using dal.Repositories;
using System;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace winui
{
    public partial class EditEmployeeForm : Form
    {
        readonly ViewModel.Employee _employee;
        public EditEmployeeForm(int employeeId)
        {
            InitializeComponent();
            _employee = LoadEmployee(employeeId);
        }

        private void uxOk_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateEmployee();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }

        private ViewModel.Employee LoadEmployee(int id)
        {
            var connection = ConfigurationManager.ConnectionStrings["EmployeeConnection"].ConnectionString;
            var repo = new RepositoryFactory().GetEmployeeRepository();
            var dbEmployee = repo.GetEmployee(id);

            return new ViewModel.Employee
            {
                Id = dbEmployee.Id,
                Position = new ViewModel.Position
                {
                    Id = dbEmployee.Position.Id,
                    Name = dbEmployee.Position.Name,
                },
                Name = dbEmployee.Name,
                DateOfEmployment = dbEmployee.DateOfEmployment,
                Surname = dbEmployee.Surname,
                Salary = dbEmployee.Salary,
                Skills = dbEmployee.EmployeeSkills
                                    .Select(s => new ViewModel.Skill
                                    {
                                        Id = s.Id,
                                        Name = s.Skill.Name
                                    })
                                    .ToList()
            };
        }

        private void LoadPositionsLookup()
        {
            var repo = new RepositoryFactory().GetPositionRepository();
            var positions = repo.GetPositions()
                                .Select(x =>
                                    new ViewModel.Position
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Skills = x.PositionSkills
                                                  .Select(s => new ViewModel.Skill
                                                  {
                                                      Id = s.Id,
                                                      Name = s.Skill.Name
                                                  })
                                                  .ToList()
                                    })
                                  .ToList();

            uxPositions.DataSource = positions;
            uxPositions.DisplayMember = "Name";
        }

        private void UpdateEmployee()
        {
            var employee = new Employee
            {
                Id = _employee.Id,
                Name = uxName.Text,
                Surname = uxSurname.Text,
                DateOfEmployment = uxEmploymentDate.Value.Date,
                Salary = uxSalary.Value,
                PositionId = ((ViewModel.Position)uxPositions.SelectedItem).Id
            };

            var employeeSkills = uxSkillsSelector.SelectedSkills
                .Select(x => new EmployeeSkill
                {
                    EmployeeId = employee.Id,
                    SkillId = x.Id
                }).ToList();

            employee.EmployeeSkills.Assign(employeeSkills);

            var repo = new RepositoryFactory().GetEmployeeRepository();
            repo.UpdateEmployee(employee);
        }

        private void uxPositions_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                var position = uxPositions.SelectedItem as ViewModel.Position;

                if (position == null)
                    return;

                uxSkillsSelector.LoadEmployeeSkills(position.Skills, _employee.Skills);
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }

        private void EditEmployeeForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadPositionsLookup();

                uxName.Text = _employee.Name;
                uxSurname.Text = _employee.Surname;
                uxEmploymentDate.Value = _employee.DateOfEmployment;
                uxSalary.Value = _employee.Salary;
                uxPositions.SelectedItem = _employee.Position;
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }
    }
}
