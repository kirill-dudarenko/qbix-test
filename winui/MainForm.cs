﻿using dal.Repositories;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace winui
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            SetGridProperties();
        }

        private void SetGridProperties()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.Columns.Clear();

            DataGridViewColumn column = new DataGridViewTextBoxColumn
            {
                Name = "Id",
                DataPropertyName = "Id",
                Visible = false
            };
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn
            {
                HeaderText = "Name",
                Name = "Name",
                DataPropertyName = "Name"
            };
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn
            {
                HeaderText = "Surname",
                Name = "Surname",
                DataPropertyName = "Surname"
            };
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn
            {
                HeaderText = "Employment Date",
                Name = "DateOfEmployment",
                DataPropertyName = "DateOfEmployment"
            };
            column.DefaultCellStyle.Format = "d";
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn
            {
                HeaderText = "Position",
                Name = "Position",
                DataPropertyName = "PositionName"
            };
            dataGridView1.Columns.Add(column);

            column = new DataGridViewTextBoxColumn
            {
                HeaderText = "Salary",
                Name = "Salary",
                DataPropertyName = "Salary"
            };
            column.DefaultCellStyle.Format = "c";
            dataGridView1.Columns.Add(column);
        }

        private void LoadEmployees()
        {
            var repo = new RepositoryFactory().GetEmployeeRepository();
            var employees = repo.GetEmployees()
                                .Select(x =>
                                    new ViewModel.Employee
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Surname = x.Surname,
                                        DateOfEmployment = x.DateOfEmployment,
                                        PositionName = x.Position.Name,
                                        Salary = x.Salary
                                    })
                                .ToList();

            dataGridView1.DataSource = new BindingList<ViewModel.Employee>(employees);
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void DeleteEmployee(int id)
        {
            new RepositoryFactory()
                .GetEmployeeRepository()
                .DeleteEmployee(id);
        }

        private void uxDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Please select row(s) to delete.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                var result = MessageBox.Show("Data will be deleted permanently, are you sure ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.No)
                    return;
                
                foreach (DataGridViewRow item in dataGridView1.SelectedRows)
                {
                    var id = Convert.ToInt32(item.Cells["Id"].Value);
                    DeleteEmployee(id);
                }
                LoadEmployees();
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadEmployees();
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }

        private void uxUpdate_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select row to edit.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var employeeId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["Id"].Value);
            var employeeManagement = new EditEmployeeForm(employeeId);
            ProcessDialogForm(employeeManagement);
        }

        private void uxCreate_Click(object sender, EventArgs e)
        {
            var employeeManagement = new NewEmployeeForm();
            ProcessDialogForm(employeeManagement);
        }

        private void ProcessDialogForm(Form form)
        {
            var dialogResult = form.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                LoadEmployees();
            }
        }
    }
}
