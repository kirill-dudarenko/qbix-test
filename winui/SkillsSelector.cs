﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace winui
{
    public partial class SkillsSelector : UserControl
    {
        public SkillsSelector()
        {
            InitializeComponent();
        }

        public void LoadPositionSkills(List<ViewModel.Skill> skills)
        {
            Controls.Clear();

            int i = 1;
            foreach (var skill in skills)
            {
                var skillSelector = new CheckBox();
                skillSelector.Name = $"skill_{skill.Id}";
                skillSelector.Text = skill.Name;
                skillSelector.Size = new System.Drawing.Size(Size.Width, 20);
                skillSelector.Location = new System.Drawing.Point(Location.X, Location.Y + 20 * i++);
                skillSelector.Tag = skill;
                Controls.Add(skillSelector);
            }
        }

        public void LoadEmployeeSkills(List<ViewModel.Skill> positionSkills, List<ViewModel.Skill> employeeSkills)
        {
            LoadPositionSkills(positionSkills);

            foreach (CheckBox item in Controls)
            {
                var positionSkill = item.Tag as ViewModel.Skill;

                if (positionSkill == null)
                    continue;

                var employeeSkill = employeeSkills.Find(x => string.Equals(x.Name, positionSkill.Name, StringComparison.OrdinalIgnoreCase));

                if (employeeSkill == null)
                    continue;

                item.Checked = true;
            }
        }

        public List<ViewModel.Skill> SelectedSkills
        {
            get
            {
                var result = new List<ViewModel.Skill>();
                foreach (Control control in Controls)
                {
                    var skillSelector = control as CheckBox;

                    if (skillSelector == null)
                        continue;

                    if (!skillSelector.Checked)
                        continue;

                    result.Add(skillSelector.Tag as ViewModel.Skill);
                }

                return result;
            }
        }
    }
}
