﻿using dal.Entities;
using dal.Repositories;
using System;
using System.Linq;
using System.Windows.Forms;

namespace winui
{
    public partial class NewEmployeeForm : Form
    {
        public NewEmployeeForm()
        {
            InitializeComponent();
        }

        private void NewEmployeeForm_Load(object sender, System.EventArgs e)
        {
            try
            {
                LoadPositionsLookup();
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }

        private void uxOk_Click(object sender, EventArgs e)
        {
            try
            {
                CreateEmployee();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }

        private void LoadPositionsLookup()
        {
            var repo = new RepositoryFactory().GetPositionRepository();
            var positions = repo.GetPositions()
                                .Select(x =>
                                    new ViewModel.Position
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Skills = x.PositionSkills
                                                  .Select(s => new ViewModel.Skill
                                                  {
                                                      Id = s.Id,
                                                      Name = s.Skill.Name
                                                  })
                                                  .ToList()
                                    })
                                  .ToList();

            uxPositions.DataSource = positions;
            uxPositions.DisplayMember = "Name";
        }

        private void CreateEmployee()
        {
            var employee = new Employee
            {
                Name = uxName.Text,
                Surname = uxSurname.Text,
                DateOfEmployment = uxEmploymentDate.Value.Date,
                Salary = uxSalary.Value,
                PositionId = ((ViewModel.Position)uxPositions.SelectedItem).Id
            };

            var employeeSkills = uxSkillsSelector.SelectedSkills
                .Select(x => new EmployeeSkill
                {
                    EmployeeId = employee.Id,
                    SkillId = x.Id
                }).ToList();

            employee.EmployeeSkills.Assign(employeeSkills);

            var repo = new RepositoryFactory().GetEmployeeRepository();
            repo.AddEmployee(employee);
        }

        private void uxPositions_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                var position = uxPositions.SelectedItem as ViewModel.Position;

                if (position == null)
                    return;

                uxSkillsSelector.LoadPositionSkills(position.Skills);
            }
            catch (Exception ex)
            {
                new HandleErrorControl(ex).LogAndNotify();
            }
        }
    }
}
