﻿using dal.Repositories;
using System;
using System.Configuration;
using System.Windows.Forms;

namespace winui
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Logger.InitLogger();
            RepositoryFactory.InitConnectionStore(
                ConfigurationManager.ConnectionStrings["EmployeeConnection"].ConnectionString);
            Application.Run(new MainForm());
        }
    }
}
