﻿using System.Collections.Generic;

namespace winui.ViewModel
{
    public class Position
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ViewModel.Skill> Skills { get; set; } = new List<ViewModel.Skill>();

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ViewModel.Position))
                return base.Equals(obj);

            var other = ((ViewModel.Position)obj);
            return Id == other.Id;
        }
    }
}
