﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winui.ViewModel
{
	public class Employee
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public System.DateTime DateOfEmployment { get; set; }
		public decimal Salary { get; set; }
		public string PositionName { get; set; }
		public List<ViewModel.Skill> Skills { get; set; } = new List<ViewModel.Skill>();
		public Position Position { get; set; }
	}
}
