﻿using System;
using System.Windows.Forms;

namespace winui
{
    public class HandleErrorControl
    {
        private readonly Exception _exception;

        public HandleErrorControl(Exception exception)
        {
            _exception = exception;
        }

        public void LogAndNotify()
        {
            Logger.Log.Error(_exception);
            MessageBox.Show(_exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
